﻿namespace Project_OOP
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddCustomer = new System.Windows.Forms.Button();
            this.AddProduct = new System.Windows.Forms.Button();
            this.AddOrder = new System.Windows.Forms.Button();
            this.ShowCustomer = new System.Windows.Forms.Button();
            this.ShowProducts = new System.Windows.Forms.Button();
            this.ShowOrders = new System.Windows.Forms.Button();
            this.labelInformations = new System.Windows.Forms.Label();
            this.exportProductsXml = new System.Windows.Forms.Button();
            this.exportCustomersXml = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AddCustomer
            // 
            this.AddCustomer.Location = new System.Drawing.Point(50, 30);
            this.AddCustomer.Name = "AddCustomer";
            this.AddCustomer.Size = new System.Drawing.Size(112, 45);
            this.AddCustomer.TabIndex = 0;
            this.AddCustomer.Text = "Dodaj Klienta";
            this.AddCustomer.UseVisualStyleBackColor = true;
            this.AddCustomer.Click += new System.EventHandler(this.button1_Click);
            // 
            // AddProduct
            // 
            this.AddProduct.Location = new System.Drawing.Point(204, 30);
            this.AddProduct.Name = "AddProduct";
            this.AddProduct.Size = new System.Drawing.Size(112, 45);
            this.AddProduct.TabIndex = 1;
            this.AddProduct.Text = "Dodaj Produkt";
            this.AddProduct.UseVisualStyleBackColor = true;
            this.AddProduct.Click += new System.EventHandler(this.AddProduct_Click);
            // 
            // AddOrder
            // 
            this.AddOrder.Location = new System.Drawing.Point(356, 30);
            this.AddOrder.Name = "AddOrder";
            this.AddOrder.Size = new System.Drawing.Size(112, 45);
            this.AddOrder.TabIndex = 2;
            this.AddOrder.Text = "Dodaj Zamówienie";
            this.AddOrder.UseVisualStyleBackColor = true;
            this.AddOrder.Click += new System.EventHandler(this.AddOrder_Click);
            // 
            // ShowCustomer
            // 
            this.ShowCustomer.Location = new System.Drawing.Point(50, 81);
            this.ShowCustomer.Name = "ShowCustomer";
            this.ShowCustomer.Size = new System.Drawing.Size(112, 45);
            this.ShowCustomer.TabIndex = 3;
            this.ShowCustomer.Text = "Pokaż Klientów";
            this.ShowCustomer.UseVisualStyleBackColor = true;
            this.ShowCustomer.Click += new System.EventHandler(this.ShowCustomer_Click);
            // 
            // ShowProducts
            // 
            this.ShowProducts.Location = new System.Drawing.Point(204, 81);
            this.ShowProducts.Name = "ShowProducts";
            this.ShowProducts.Size = new System.Drawing.Size(112, 45);
            this.ShowProducts.TabIndex = 4;
            this.ShowProducts.Text = "Pokaż Produkty";
            this.ShowProducts.UseVisualStyleBackColor = true;
            this.ShowProducts.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ShowOrders
            // 
            this.ShowOrders.Location = new System.Drawing.Point(356, 81);
            this.ShowOrders.Name = "ShowOrders";
            this.ShowOrders.Size = new System.Drawing.Size(112, 45);
            this.ShowOrders.TabIndex = 5;
            this.ShowOrders.Text = "Pokaż Zamówienia";
            this.ShowOrders.UseVisualStyleBackColor = true;
            this.ShowOrders.Click += new System.EventHandler(this.ShowOrders_Click);
            // 
            // labelInformations
            // 
            this.labelInformations.AutoSize = true;
            this.labelInformations.Location = new System.Drawing.Point(47, 142);
            this.labelInformations.Name = "labelInformations";
            this.labelInformations.Size = new System.Drawing.Size(68, 13);
            this.labelInformations.TabIndex = 6;
            this.labelInformations.Text = "Informacje ...";
            this.labelInformations.Click += new System.EventHandler(this.labelInformations_Click);
            // 
            // exportProductsXml
            // 
            this.exportProductsXml.Cursor = System.Windows.Forms.Cursors.Cross;
            this.exportProductsXml.Location = new System.Drawing.Point(646, 81);
            this.exportProductsXml.Name = "exportProductsXml";
            this.exportProductsXml.Size = new System.Drawing.Size(112, 45);
            this.exportProductsXml.TabIndex = 7;
            this.exportProductsXml.TabStop = false;
            this.exportProductsXml.Text = "Eksport produktów do XML";
            this.exportProductsXml.UseVisualStyleBackColor = true;
            this.exportProductsXml.Click += new System.EventHandler(this.exportProductsXml_Click);
            // 
            // exportCustomersXml
            // 
            this.exportCustomersXml.Cursor = System.Windows.Forms.Cursors.Cross;
            this.exportCustomersXml.Location = new System.Drawing.Point(646, 30);
            this.exportCustomersXml.Name = "exportCustomersXml";
            this.exportCustomersXml.Size = new System.Drawing.Size(112, 45);
            this.exportCustomersXml.TabIndex = 8;
            this.exportCustomersXml.TabStop = false;
            this.exportCustomersXml.Text = "Eksport klientów do XML";
            this.exportCustomersXml.UseVisualStyleBackColor = true;
            this.exportCustomersXml.Click += new System.EventHandler(this.exportCustomersXml_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelInformations);
            this.Controls.Add(this.exportCustomersXml);
            this.Controls.Add(this.exportProductsXml);
            this.Controls.Add(this.ShowOrders);
            this.Controls.Add(this.ShowProducts);
            this.Controls.Add(this.ShowCustomer);
            this.Controls.Add(this.AddOrder);
            this.Controls.Add(this.AddProduct);
            this.Controls.Add(this.AddCustomer);
            this.Name = "Form1";
            this.Text = "Prototyp CRM";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddCustomer;
        private System.Windows.Forms.Button AddProduct;
        private System.Windows.Forms.Button AddOrder;
        private System.Windows.Forms.Button ShowCustomer;
        private System.Windows.Forms.Button ShowProducts;
        private System.Windows.Forms.Button ShowOrders;
        private System.Windows.Forms.Label labelInformations;
        private System.Windows.Forms.Button exportProductsXml;
        private System.Windows.Forms.Button exportCustomersXml;
    }
}

