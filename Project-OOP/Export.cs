﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;



namespace Project_OOP
{
    
    class Export
    {

        
        public void ExportCustomer()
        {
            List<Customer> customers = Program.customers;
            

            if( customers.Count > 0 ) // only if any customer exists
            {
                Array customerAttributes =  customers.First().customerAttributesForExport(); // get first array from method customeAttributes
                // FILE HANDLING
                string desktopPath = Environment.GetFolderPath( Environment.SpecialFolder.Desktop ); // desktop path
                string filePath = desktopPath + "\\exported_customers.txt"; // path of file with all necessary details
            
                if( File.Exists( filePath ) ) // if this file exists
                {
                    File.Delete( filePath ); // delete file
                }
                // END OF FILE HANDLING

            

                // EXPORT
                string indent = ""; 
            
                using( StreamWriter sw = File.AppendText( filePath ) ){ // create file
                    sw.WriteLine( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" ); // xml starting something
                    int index = 0; // index for indexing in customerAttributes
                    foreach( Customer cust in Program.customers ) 
                    {
                        index = 0; // reset index
                        sw.WriteLine( indent + "<Customer>" );
                        indent = "  ";

                        // create <NAMEOFATTRIBUTE>VALUEOFATTRIBUTE</NAMEOFATTRIBUTE>
                        sw.WriteLine( indent + '<' + customerAttributes.GetValue(index).ToString() + '>' + cust.Id + "</" + customerAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + customerAttributes.GetValue(index).ToString() + '>' + cust.Name + "</" + customerAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + customerAttributes.GetValue(index).ToString() + '>' + cust.Surname + "</" + customerAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + customerAttributes.GetValue(index).ToString() + '>' + cust.Address + "</" + customerAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + customerAttributes.GetValue(index).ToString() + '>' + cust.Age + "</" + customerAttributes.GetValue(index++).ToString() + '>');
                    
                        sw.WriteLine( "</Customer>" );
                        indent = "";
                    }
                }
            }
            else
            {
                MessageBox.Show( "Brak klientów do eksportu" );
            }
            // END OF EXPORT
        }
        
        public void ExportProduct()
        {
            List<Product> products = Program.products;
            

            if( products.Count > 0 ) // only if any product exists
            {
                Array productAttributes =  products.First().productsAttributesForExport(); // get first array from method customeAttributes
                // FILE HANDLING
                string desktopPath = Environment.GetFolderPath( Environment.SpecialFolder.Desktop ); // desktop path
                string filePath = desktopPath + "\\exported_products.txt"; // path of file with all necessary details
            
                if( File.Exists( filePath ) ) // if this file exists
                {
                    File.Delete( filePath ); // delete file
                }
                // END OF FILE HANDLING

            

                // EXPORT
                string indent = ""; 
            
                using( StreamWriter sw = File.AppendText( filePath ) ){ // create file
                    sw.WriteLine( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" ); // xml starting something
                    int index = 0; // index for indexing in customerAttributes
                    foreach( Product prod in Program.products ) 
                    {
                        index = 0; // reset index
                        sw.WriteLine( indent + "<Product>" );
                        indent = "  ";

                        // create <NAMEOFATTRIBUTE>VALUEOFATTRIBUTE</NAMEOFATTRIBUTE>
                        sw.WriteLine( indent + '<' + productAttributes.GetValue(index).ToString() + '>' + prod.Id + "</" + productAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + productAttributes.GetValue(index).ToString() + '>' + prod.Name + "</" + productAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + productAttributes.GetValue(index).ToString() + '>' + prod.Price + "</" + productAttributes.GetValue(index++).ToString() + '>');
                        sw.WriteLine( indent + '<' + productAttributes.GetValue(index).ToString() + '>' + prod.Quantity + "</" + productAttributes.GetValue(index++).ToString() + '>');
                        
                        sw.WriteLine( "</Product>" );
                        indent = "";
                    }
                }
            }
            else
            {
                MessageBox.Show( "Brak produktów do eksportu" );
            }
            // END OF EXPORT
        }
    }
}
