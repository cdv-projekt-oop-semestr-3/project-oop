﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_OOP
{
    public partial class FormCustomer : Form
    {
        public FormCustomer()
        {
            InitializeComponent();
        }

        private void customerAdd_Click(object sender, EventArgs e)
        {

            if (customerName.TextLength == 0 || 
                customerSurname.TextLength == 0 || 
                customerAddress.TextLength == 0 || 
                customerAge.TextLength == 0)
            {
                MessageBox.Show("Uzupełnij wszystkie pola");
            }
            else
            {
                //Validate Data
                int _customerAge;

                if (!int.TryParse(customerAge.Text, out _customerAge))
                {
                    MessageBox.Show("Wiek musi być liczbą.");
                    return;
                }

                //Create Customer object
                Customer newCustomer = new Customer(
                    this.generateCustomerId(),
                    customerName.Text,
                    customerSurname.Text,
                    customerAddress.Text,
                    _customerAge
                );

                //Add Customer to list
                Program.customers.Add(newCustomer);

                //Display dialog box - operation has been processed properly
                MessageBox.Show("Operacja zakończona sukcesem.\n" + newCustomer.printCustomer());

                //Clear fileds
                if (!this.clearFields())
                    MessageBox.Show("Eror clearing fileds.");

            }
        }

        private int generateCustomerId()
        {
            if (Program.customers.Count == 0)
                return 0; 
            else
                return Program.customers.Count;
        }
        private bool clearFields()
        {
            customerName.Text = "";
            customerSurname.Text = "";
            customerAddress.Text = "";
            customerAge.Text = "";

            return true;
        }
    }
}
