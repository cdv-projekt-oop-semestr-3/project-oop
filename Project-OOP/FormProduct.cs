﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_OOP
{
    public partial class FormProducts : Form
    {
        public FormProducts()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            
            int _price;
            int _quantity;

            if (productName.TextLength == 0 ||
                productPrice.TextLength == 0 ||
                productQuantity.TextLength == 0)
            {
                MessageBox.Show("Uzupełnij wszytkie pola");
            }
            else if (!int.TryParse(productPrice.Text, out _price))
            {
                MessageBox.Show("Cena musi być liczbą.");
                return;
            }
            else if (!int.TryParse(productQuantity.Text, out _quantity))
            {
                MessageBox.Show("Ilość musi być liczbą.");
                return;
            }
            else
            {
                Product newProduct = new Product(
                    this.generateProductsId(),
                    _price,
                    productName.Text,
                    _quantity
                );

                Program.products.Add(newProduct);

                MessageBox.Show("Operacja zakończona sukcesem.\n" + newProduct.printProducts());

                if (!this.clearFields())
                    MessageBox.Show("Eror clearing fileds.");



            }

        }

        private int generateProductsId()
        {
            if (Program.products.Count == 0)
                return 0;
            else
                return Program.products.Count;
        }

        private bool clearFields()
        {
            productName.Text = "";
            productPrice.Text = "";
            productQuantity.Text = "";
            

            return true;
        }


    }
}
