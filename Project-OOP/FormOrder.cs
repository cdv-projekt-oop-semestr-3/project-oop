﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_OOP
{
    public partial class FormOrder : Form
    {
        public FormOrder()
        {
            InitializeComponent();
        }

        private void FormOrder_Load(object sender, EventArgs e)
        {

            //Load customer data
            foreach(Customer customer in Program.customers)
            {
                comboBoxCustomers.Items.Insert(customer.Id, customer.Name + " " + customer.Surname);
            }

            //Load products data
            foreach(Product product in Program.products)
            {
                checkedListBoxProducts.Items.Insert(product.Id, product.Name + " - " + product.Price + " zł" );
            }
        }

        private void buttonCreateOrder_Click(object sender, EventArgs e)
        {
            //Prepare data
            int customerId = comboBoxCustomers.SelectedIndex;
            int[] selectedProducts = checkedListBoxProducts.CheckedIndices.Cast<int>().ToArray();

            //Validate are data selected
            if (customerId == -1 || checkedListBoxProducts.CheckedIndices.Count == 0)
            {
                MessageBox.Show("Wybierz klienta oraz produkty.");
                return;
            }

            //Create object
            Order order = new Order(this.generateOrderId(), customerId, selectedProducts);

            //Add order to list
            Program.orders.Add(order);

            //Show comment
            MessageBox.Show("Zamówienie utworzone poprawnie");

            //Clear selections for customer
            comboBoxCustomers.SelectedIndex = -1;

            //Clear selections for products
            foreach (int selectedProduct in checkedListBoxProducts.CheckedIndices)
            {
                checkedListBoxProducts.SetItemCheckState(selectedProduct, CheckState.Unchecked);
            }

        }
        private int generateOrderId()
        {
            if (Program.orders.Count == 0)
                return 0;
            else
                return Program.orders.Count;
        }
    }
}
