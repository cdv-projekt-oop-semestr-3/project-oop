﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_OOP
{
    class Customer
    {
        int id, age;
        string name, surname, address; 

        public Customer(int _id, string _name, string _surname, string _address, int _age)
        {
            id = _id;
            name = _name;
            surname = _surname;
            address = _address;
            age = _age;
        }

        /*
         * Methods
         */
        public Array customerAttributesForExport()
        {
            string[] attriburtesForExport =
            {
                "id",
                "name",
                "surname",
                "address",
                "age"
            };

            return attriburtesForExport;
        }

        public string printCustomer()
        {
            return "ID: " + this.id + " - " + this.Name + " " + this.Surname + ", wiek: " + this.Age + ", adres: " + this.Address; 
        }

        /*
         * Getters and setters 
         */
        public int Id
        {
            get { return id; }
        }

        public int Age
        {
            get { return age; }

            private set
            { age = value; }
        }

        public string Name
        {
            get { return name;  }

            private set { name = value; }
        }
        public string Surname
        {
            get { return surname; }

            private set { surname = value; }
        }
        public string Address
        {
            get { return address; }

            private set { address = value; }
        }
    }
}
