﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_OOP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        { 
            FormCustomer formCustomer = new FormCustomer();
            formCustomer.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (!Program.products.Any())
            {
                mainScreenShowInfo("Brak produktow");
                return;
            }

            string text = "";

            foreach (Product product in Program.products)
            {
                text += product.printProducts() + "\n";
            }
            mainScreenShowInfo(text);
        }

        private void AddProduct_Click(object sender, EventArgs e)
        {
            FormProducts formProducts = new FormProducts();
            formProducts.Show();
        }
        
        private void ShowOrders_Click(object sender, EventArgs e)
        {
            if( Program.orders.Count > 0 )
            {
                string retString = "";
                foreach( Order order in Program.orders )
                {
                    retString += order.printOrder();
                    retString += '\n';
                }
                mainScreenShowInfo( retString );
            }
            else
            {
                mainScreenShowInfo( "Brak zamówień do wyświetlenia" );
            }

        }

        private void ShowCustomer_Click(object sender, EventArgs e)
        {
            if(!Program.customers.Any())
            {
                mainScreenShowInfo("Brak klientów");
                return;
            }

            string text = ""; 

            foreach(Customer customer in Program.customers)
            {
                text += customer.printCustomer() + "\n";
            }
            mainScreenShowInfo(text);
        }

        private void mainScreenShowInfo(string info)
        {
            this.labelInformations.Text = info;
        }

        private void button1_Click_2(object sender, EventArgs e) {

        }


        private void exportProductsXml_Click(object sender, EventArgs e) {
            Export export = new Export();
            export.ExportProduct();
        }

        private void exportCustomersXml_Click(object sender, EventArgs e) {
            Export export = new Export();
            export.ExportCustomer();

        }

        private void labelInformations_Click(object sender, EventArgs e)
        {

        }

        private void AddOrder_Click(object sender, EventArgs e)
        {
            //Check if neaded data exist
            if (Program.customers.Count != 0 && Program.products.Count != 0)
            {
                //Create new form
                FormOrder formOrder = new FormOrder();

                //Show form on screen
                formOrder.Show();
            }
            else
            {
                MessageBox.Show("Brak klientów lub produktów.");
                return;
            }

           
        }
    }
}
