﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_OOP
{
    class Product
    {
        int id, price, quantity;
        string name;

        public Product( int _id, int _price, string _name, int _quantity)
        {
            id = _id;
            price = _price;
            Name = _name;
            quantity = _quantity;
        }

        public Array productsAttributesForExport()
        {
            string[] attributesForExport =
            {
                "id",
                "price",
                "name",
                "quantity",
            };

            return attributesForExport;
        }

        public string printProducts()
        {
            return "ID: " + this.id + " - " + this.name + " cena: " + this.price + " ilosc: " + this.quantity;   
        }


        public int Id
        {
            get { return id; }
        }

        public int Price
        {
            get { return price; }

            private set
            { price = value; }
        }

        public string Name
        {
            get { return name; }

            private set { name = value; }
        }
        public int Quantity
        {
            get { return quantity; }

            private set { quantity = value; }
        }

    }
}
