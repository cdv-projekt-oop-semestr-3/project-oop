﻿namespace Project_OOP
{
    partial class FormCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customerName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.customerSurname = new System.Windows.Forms.TextBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.customerAddress = new System.Windows.Forms.TextBox();
            this.labelAge = new System.Windows.Forms.Label();
            this.customerAge = new System.Windows.Forms.TextBox();
            this.customerAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customerName
            // 
            this.customerName.Location = new System.Drawing.Point(57, 48);
            this.customerName.Name = "customerName";
            this.customerName.Size = new System.Drawing.Size(100, 20);
            this.customerName.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(57, 29);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(26, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Imię";
            // 
            // customerSurname
            // 
            this.customerSurname.Location = new System.Drawing.Point(57, 95);
            this.customerSurname.Name = "customerSurname";
            this.customerSurname.Size = new System.Drawing.Size(100, 20);
            this.customerSurname.TabIndex = 2;
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(57, 75);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(53, 13);
            this.labelSurname.TabIndex = 3;
            this.labelSurname.Text = "Nazwisko";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(57, 122);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(34, 13);
            this.labelAddress.TabIndex = 4;
            this.labelAddress.Text = "Adres";
            // 
            // customerAddress
            // 
            this.customerAddress.Location = new System.Drawing.Point(57, 139);
            this.customerAddress.Name = "customerAddress";
            this.customerAddress.Size = new System.Drawing.Size(100, 20);
            this.customerAddress.TabIndex = 5;
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Location = new System.Drawing.Point(57, 166);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(32, 13);
            this.labelAge.TabIndex = 6;
            this.labelAge.Text = "Wiek";
            // 
            // customerAge
            // 
            this.customerAge.Location = new System.Drawing.Point(57, 183);
            this.customerAge.Name = "customerAge";
            this.customerAge.Size = new System.Drawing.Size(100, 20);
            this.customerAge.TabIndex = 7;
            // 
            // customerAdd
            // 
            this.customerAdd.Location = new System.Drawing.Point(57, 231);
            this.customerAdd.Name = "customerAdd";
            this.customerAdd.Size = new System.Drawing.Size(75, 23);
            this.customerAdd.TabIndex = 8;
            this.customerAdd.Text = "Utwórz";
            this.customerAdd.UseVisualStyleBackColor = true;
            this.customerAdd.Click += new System.EventHandler(this.customerAdd_Click);
            // 
            // FormCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.customerAdd);
            this.Controls.Add(this.customerAge);
            this.Controls.Add(this.labelAge);
            this.Controls.Add(this.customerAddress);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.customerSurname);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.customerName);
            this.Name = "FormCustomer";
            this.Text = "Dodawanie klienta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox customerName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox customerSurname;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.TextBox customerAddress;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.TextBox customerAge;
        private System.Windows.Forms.Button customerAdd;
    }
}