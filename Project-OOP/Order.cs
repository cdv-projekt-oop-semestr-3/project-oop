﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_OOP {
    class Order {
        int id, customerId;
        int[] orderedProducts;
        
        public Order( int _id, int _customerId, int[] _orderedProducts )
        {
            id = _id;
            customerId = _customerId;
            orderedProducts = _orderedProducts;
        }
        
        public int Id { set; get; }
        public int CustomerId { set; get; }
        public int OrderedProducts { set; get; }

        public string printOrder()
        {
            string products = "";
            foreach( int prod in orderedProducts )
            {
                products += Program.products[prod].Name;
                products += ' ';
            }

            return ( "Zamówienie nr " + this.id + " dla " + 
            Program.customers[customerId].Name + ' ' + Program.customers[customerId].Surname + ", który zamówił produkty: " + 
            products
            );

        }
    }
}