﻿namespace Project_OOP
{
    partial class FormOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxCustomers = new System.Windows.Forms.ComboBox();
            this.labelComboBoxCustomer = new System.Windows.Forms.Label();
            this.buttonCreateOrder = new System.Windows.Forms.Button();
            this.checkedListBoxProducts = new System.Windows.Forms.CheckedListBox();
            this.labelSelectProducts = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxCustomers
            // 
            this.comboBoxCustomers.FormattingEnabled = true;
            this.comboBoxCustomers.Location = new System.Drawing.Point(33, 71);
            this.comboBoxCustomers.Name = "comboBoxCustomers";
            this.comboBoxCustomers.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCustomers.TabIndex = 0;
            // 
            // labelComboBoxCustomer
            // 
            this.labelComboBoxCustomer.AutoSize = true;
            this.labelComboBoxCustomer.Location = new System.Drawing.Point(33, 52);
            this.labelComboBoxCustomer.Name = "labelComboBoxCustomer";
            this.labelComboBoxCustomer.Size = new System.Drawing.Size(79, 13);
            this.labelComboBoxCustomer.TabIndex = 1;
            this.labelComboBoxCustomer.Text = "Wybierz klienta";
            // 
            // buttonCreateOrder
            // 
            this.buttonCreateOrder.Location = new System.Drawing.Point(33, 352);
            this.buttonCreateOrder.Name = "buttonCreateOrder";
            this.buttonCreateOrder.Size = new System.Drawing.Size(75, 23);
            this.buttonCreateOrder.TabIndex = 2;
            this.buttonCreateOrder.Text = "Utwórz";
            this.buttonCreateOrder.UseVisualStyleBackColor = true;
            this.buttonCreateOrder.Click += new System.EventHandler(this.buttonCreateOrder_Click);
            // 
            // checkedListBoxProducts
            // 
            this.checkedListBoxProducts.FormattingEnabled = true;
            this.checkedListBoxProducts.Location = new System.Drawing.Point(192, 71);
            this.checkedListBoxProducts.Name = "checkedListBoxProducts";
            this.checkedListBoxProducts.Size = new System.Drawing.Size(368, 184);
            this.checkedListBoxProducts.TabIndex = 3;
            // 
            // labelSelectProducts
            // 
            this.labelSelectProducts.AutoSize = true;
            this.labelSelectProducts.Location = new System.Drawing.Point(192, 52);
            this.labelSelectProducts.Name = "labelSelectProducts";
            this.labelSelectProducts.Size = new System.Drawing.Size(89, 13);
            this.labelSelectProducts.TabIndex = 4;
            this.labelSelectProducts.Text = "Wybierz produkty";
            // 
            // FormOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelSelectProducts);
            this.Controls.Add(this.checkedListBoxProducts);
            this.Controls.Add(this.buttonCreateOrder);
            this.Controls.Add(this.labelComboBoxCustomer);
            this.Controls.Add(this.comboBoxCustomers);
            this.Name = "FormOrder";
            this.Text = "Dodawanie zamówienia";
            this.Load += new System.EventHandler(this.FormOrder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxCustomers;
        private System.Windows.Forms.Label labelComboBoxCustomer;
        private System.Windows.Forms.Button buttonCreateOrder;
        private System.Windows.Forms.CheckedListBox checkedListBoxProducts;
        private System.Windows.Forms.Label labelSelectProducts;
    }
}